package hr.fer.tel.ruazosa.FriendsLocationService.FriendLocatonServices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FriendLocatonServicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(FriendLocatonServicesApplication.class, args);
	}

}
