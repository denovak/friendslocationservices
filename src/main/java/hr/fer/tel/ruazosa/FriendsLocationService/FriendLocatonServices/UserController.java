package hr.fer.tel.ruazosa.FriendsLocationService.FriendLocatonServices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@RestController
public class UserController {
    @Autowired
    IUserService userService;

    @GetMapping("/users")
    public Page<User> findUsers(@ModelAttribute(value="from") int from, @ModelAttribute(value="size") int size) {
        return userService.findAll(from, size);
    }

    @PostMapping(path = "/users")
    public ResponseEntity<Object> registerUser(@RequestBody User user) {

        if (userService.checkIfUsernameIsTaken(user)) {
            Map<String, Object> body = new LinkedHashMap<>();
            body.put("timestamp", new Date());
            body.put("status", HttpStatus.NOT_FOUND);
            body.put("errors", "Username already exists");
            return new ResponseEntity<Object>(body, HttpStatus.BAD_REQUEST);
        }
        userService.createNewUser(user);
        return new ResponseEntity<Object>(HttpStatus.OK);
    }

    @PostMapping(path = "/users/login")
    public ResponseEntity<Object> loginUser(@RequestBody User user) {
        if (!userService.loginInUser(user.getUserName(), user.getPassword())) {
            Map<String, Object> body = new LinkedHashMap<>();
            body.put("timestamp", new Date());
            body.put("status", HttpStatus.NOT_FOUND);
            body.put("errors", "No such username or password");
            return new ResponseEntity<Object>(body, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Object>(HttpStatus.OK);
    }
}
