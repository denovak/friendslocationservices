package hr.fer.tel.ruazosa.FriendsLocationService.FriendLocatonServices;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, Long> {
    @Query("SELECT id FROM User where userName = ?1")
    Long findByUserName(String userName);

    @Query("SELECT id FROM User where userName = ?1 and password = ?2")
    Long loginUser(String userName, String password);

}