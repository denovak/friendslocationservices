package hr.fer.tel.ruazosa.FriendsLocationService.FriendLocatonServices;

import org.springframework.data.domain.Page;

import java.util.List;

public interface IUserService {
    public void createNewUser(User user);
    public Page<User> findAll(int from, int size);
    boolean checkIfUsernameIsTaken(User user);
    public boolean loginInUser(String username, String password);
}

