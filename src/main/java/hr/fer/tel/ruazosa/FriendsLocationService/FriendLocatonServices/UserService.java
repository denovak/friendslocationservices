package hr.fer.tel.ruazosa.FriendsLocationService.FriendLocatonServices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class UserService implements IUserService{

    @Autowired
    private UserRepository repository;

    @Override
    public void createNewUser(User user) {
        repository.save(user);
    }

    @Override
    public Page<User> findAll(int from, int size) {

        return (Page<User>) repository.findAll(PageRequest.of(from, size));
    }

    @Override
    public boolean loginInUser(String username, String password) {
        if (repository.loginUser(username, password) != null) {
            return true;
        }
        return false;
    }

    @Override
    public boolean checkIfUsernameIsTaken(User user) {
        if (user == null) {
            throw new RuntimeException("User cannot be null");
        }

        if (user.getUserName() == null || user.getUserName().trim().equals("")) {
            throw new RuntimeException("User cannot be null or empty");
        }

        if (repository.findByUserName(user.getUserName()) != null) {
            return true;
        }

        return false;
    }


}

